﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainMenuBehaviour : MonoBehaviour
{
    private GameManager _manager;

    [SerializeField] private GameObject _settingsWindow;
    [SerializeField] private Slider _volumeSlider;
    [SerializeField] private TMP_Dropdown _resolutionDropdown;
    [SerializeField] private Toggle _fullScreenToggle;

    [SerializeField] private GameObject matchSetupWindow;
    [SerializeField] private TMP_Dropdown dimensionXDropdown;
    [SerializeField] private TMP_Dropdown dimensionZDropdown;

    [SerializeField] private List<Toggle> player1Chars;
    [SerializeField] private List<Toggle> player2Chars;
    
    private void Start()
    {
        _manager = GameManager.instance;
        _settingsWindow.SetActive(false);
        matchSetupWindow.SetActive(false);
    }

    public void StartNewMatch()
    {
        int dim_x = int.Parse(dimensionXDropdown.options[dimensionXDropdown.value].text); ;
        int dim_z = int.Parse(dimensionZDropdown.options[dimensionZDropdown.value].text); ;

        foreach(Toggle t in player1Chars)
        {
            if (t.isOn)
            {
                GameManager.instance.SelectPlayer1Character(player1Chars.IndexOf(t));
            }
        }
        foreach (Toggle t in player2Chars)
        {
            if (t.isOn)
            {
                GameManager.instance.SelectPlayer2Character(player2Chars.IndexOf(t));
            }
        }
        _manager.GoToScene("MatchScene", dim_x, dim_z);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void OpenSettings()
    {
        _settingsWindow.SetActive(true);

        _volumeSlider.value = _manager._settings.volume;

        _fullScreenToggle.isOn = _manager._settings.fullScreen;

        string resString = $"{_manager._settings.res_x} x {_manager._settings.res_y}";

        int currentResValue = 0;

        for (int x = 0; x < _resolutionDropdown.options.Count; x++)
        {
            TMP_Dropdown.OptionData option = _resolutionDropdown.options[x];
            if (option.text == resString)
            {
                currentResValue = x;
                break;
            }
        }

        _resolutionDropdown.value = currentResValue;
    }

    public void SaveSettings()
    {
        string resStr = _resolutionDropdown.options[_resolutionDropdown.value].text.Trim();

        string[] res = resStr.Split('x');

        int res_x = int.Parse(res[0]);
        int res_y = int.Parse(res[1]);
        bool fullscreen = _fullScreenToggle.isOn;
        float volume = _volumeSlider.value;

        _manager.SetGameConfig(res_x, res_y, fullscreen, volume);

        _settingsWindow.SetActive(false);
    }
}
