﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class BoardManager : MonoBehaviour
{
    public static BoardManager instance;

    private GameManager _gameManager;

    [SerializeField] private GameObject tilePrefab;
    [SerializeField] private Transform _boardTransform;

    private Board _board;
    public Board Board { get { return _board; } }

    [SerializeField] GameObject hpColPrefab;
    [SerializeField] GameObject atkColPrefab;
    [SerializeField] GameObject moveColPrefab;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }

        instance = this;
    }


    private void OnEnable()
    {
        CharacterManager.OnCharactersInstantiated += InstantiateCollectables;
    }

    private void OnDisable()
    {
        CharacterManager.OnCharactersInstantiated -= InstantiateCollectables;
    }

    IEnumerator Start()
    {
        yield return new WaitUntil(() => GameManager.instance != null);

        _gameManager = GameManager.instance;
        _board = new Board(_gameManager.BoardDimension_X, _gameManager.BoardDimension_Z);

        for (int x = 0; x < _gameManager.BoardDimension_X; x++)
        {
            for (int z = 0; z < _gameManager.BoardDimension_Z; z++)
            {
                GameObject tileInstance = Instantiate(tilePrefab, _boardTransform);
                tileInstance.layer = 8;
                tileInstance.transform.position = new Vector3(x * tileInstance.transform.localScale.x, 0, z * tileInstance.transform.localScale.z);
                _board.tiles[x, z].tileObj = tileInstance;
            }
        }

        OnBoardCreated.Invoke(_board);
    }

    public void InstantiateCollectables()
    {
        _board.DistributeCollectables();

        foreach (Tile tile in _board.tiles)
        {
            if (!tile.hasPlayer)
            {
                GameObject colInstance;

                if (tile.collectable is HpCollectable)
                {
                    colInstance = Instantiate(hpColPrefab);
                    colInstance.transform.position = tile.tileObj.transform.position;
                }
                else if (tile.collectable is AtkCollectable)
                {
                    colInstance = Instantiate(atkColPrefab);
                    colInstance.transform.position = tile.tileObj.transform.position;
                }
                else
                {
                    colInstance = Instantiate(moveColPrefab);
                    colInstance.transform.position = tile.tileObj.transform.position;
                }

                tile.collectable.instance = colInstance;
                colInstance.transform.SetParent(tile.tileObj.transform);
            }

        }

    }

    public List<Tile> CheckPossibleTiles(Player player)
    {
        List<Tile> possibleTiles = new List<Tile>();

        int pos_x = (int)(player.currentTile.tileObj.transform.position.x / player.currentTile.tileObj.transform.localScale.x);
        int pos_z = (int)(player.currentTile.tileObj.transform.position.z / player.currentTile.tileObj.transform.localScale.z);

        if (pos_z - 1 >= 0)
        {
            if (!_board.tiles[pos_x, pos_z - 1].hasPlayer)
            {
                possibleTiles.Add(_board.tiles[pos_x, pos_z - 1]);
            }
        }

        if (pos_z + 1 < _board.tiles.GetLength(1))
        {
            if (!_board.tiles[pos_x, pos_z + 1].hasPlayer)
            {
                possibleTiles.Add(_board.tiles[pos_x, pos_z + 1]);
            }
        }

        if (pos_x - 1 >= 0)
        {
            if (!_board.tiles[pos_x - 1, pos_z].hasPlayer)
            {
                possibleTiles.Add(_board.tiles[pos_x - 1, pos_z]);
            }
        }

        if (pos_x + 1 < _board.tiles.GetLength(0))
        {
            if (!_board.tiles[pos_x + 1, pos_z].hasPlayer)
            {
                possibleTiles.Add(_board.tiles[pos_x + 1, pos_z]);
            }
        }
        return possibleTiles;
    }

    public bool IsEnemyNearby(Player player)
    {
        int pos_x = (int)(player.currentTile.tileObj.transform.position.x / player.currentTile.tileObj.transform.localScale.x);
        int pos_z = (int)(player.currentTile.tileObj.transform.position.z / player.currentTile.tileObj.transform.localScale.z);

        bool enemyNearby = false;

        for (int x = pos_x - 1; x <= pos_x + 1; x++)
        {
            if (x >= 0 && x < _board.tiles.GetLength(0))
            {
                for (int z = pos_z - 1; z <= pos_z + 1; z++)
                {
                    if (z >= 0 && z < _board.tiles.GetLength(1))
                    {
                        if (_board.tiles[x,z].hasPlayer && player.currentTile!= _board.tiles[x,z])
                        {
                            enemyNearby = true;
                            break;
                        }
                    }
                }
            }
        }

        return enemyNearby;
    }

    public void DestroyCollectable(Collectable collectable)
    {
        GameObject colObj = collectable.instance;

        int pos_x = (int)(colObj.transform.position.x / colObj.transform.localScale.x);
        int pos_z = (int)(colObj.transform.position.z / colObj.transform.localScale.z);

        _board.tiles[pos_x, pos_z].collectable = null;

        Destroy(colObj);
    }
    public delegate void BoardEvents(Board board);
    public static event BoardEvents OnBoardCreated;
}
