﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManager : MonoBehaviour
{
    public static CharacterManager instance;

    private GameManager _gameManager;

    [SerializeField]
    private Character[] allChars = new Character[] { new Character() {hp = 100,atk = 10 },
                                                                      new Character() {hp = 150, atk = 5 },
                                                                      new Character() {hp = 70, atk = 25 },
                                                                      new Character() {hp = 120, atk = 15 } };

    [SerializeField] private GameObject[] charPrefabs;

    private Character _char1, _char2;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }

        instance = this;
    }

    private void OnEnable()
    {
        BoardManager.OnBoardCreated += InstantiateCharacters;
    }

    private void OnDisable()
    {
        BoardManager.OnBoardCreated -= InstantiateCharacters;
    }

    IEnumerator Start()
    {
        yield return new WaitUntil(() => GameManager.instance != null);

        _gameManager = GameManager.instance;

        _char1 = allChars[GameManager.instance.player1CharIndex];
        _char2 = allChars[GameManager.instance.player2CharIndex];

        for (int x = 0; x < allChars.Length; x++)
        {
            allChars[x].prefab = charPrefabs[x];
        }
    }

    private void InstantiateCharacters(Board board)
    {
        Player _player1 = new Player(_char1);
        Player _player2 = new Player(_char2);

        int p1_x = Random.Range(0, _gameManager.BoardDimension_X);
        int p1_z = Random.Range(0, _gameManager.BoardDimension_Z);

        _player1.Initialize(board.tiles[p1_x, p1_z]);

        GameObject p1Instance = Instantiate(_player1.character.prefab);
        p1Instance.transform.position = board.tiles[p1_x, p1_z].tileObj.transform.position;
        _player1.charInstance = p1Instance;

        int p2_x = Random.Range(0, _gameManager.BoardDimension_X);
        int p2_z = Random.Range(0, _gameManager.BoardDimension_Z);

        while (p2_x == p1_x && p2_z == p1_z)
        {
            p2_x = Random.Range(0, _gameManager.BoardDimension_X);
            p2_z = Random.Range(0, _gameManager.BoardDimension_Z);
        }

        _player2.Initialize(board.tiles[p2_x, p2_z]);

        GameObject p2Instance = Instantiate(_player2.character.prefab);
        p2Instance.transform.position = board.tiles[p2_x, p2_z].tileObj.transform.position;
        _player2.charInstance = p2Instance;

        OnPlayersCreated.Invoke(_player1, _player2);
        OnCharactersInstantiated.Invoke();
    }

    public delegate void BoardEvents();
    public static event BoardEvents OnCharactersInstantiated;

    public delegate void PlayerEvents(Player p1, Player p2);
    public static event PlayerEvents OnPlayersCreated;
}
