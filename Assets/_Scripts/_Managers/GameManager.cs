﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
public class GameManager : MonoBehaviour
{

    public static GameManager instance = null;

    private string _sceneToLoad;
    public string NextScene { get { return _sceneToLoad; } }

    public GameSettings _settings;
    private string _settingsPath;

    private int boardDimension_X;
    public int BoardDimension_X { get { return boardDimension_X; } }

    private int boardDimension_Z;
    public int BoardDimension_Z { get { return boardDimension_Z; } }

    public int player1CharIndex, player2CharIndex;

    private Player[] players = new Player[2];

    private Player currentPlayer;
    private Player previousPlayer;

    private int currentPlayerIndex;

    [SerializeField] private float charSpeed;

    [SerializeField] private bool battling;

    [SerializeField] Camera camera;
    [SerializeField] Transform cameraGuide;
    [SerializeField] float cameraLerpTime;

    [SerializeField] GameObject endGameWindow;
    [SerializeField] TMPro.TMP_Text endGameMessage;
    private void Awake()
    {

        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }

        instance = this;

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        endGameWindow.SetActive(false);

        _settingsPath = Path.Combine(Application.persistentDataPath, "settings.json");

        if (File.Exists(_settingsPath))
        {
            _settings = JsonUtility.FromJson<GameSettings>(File.ReadAllText(_settingsPath));
            Screen.SetResolution(_settings.res_x, _settings.res_y, _settings.fullScreen);
            AudioListener.volume = _settings.volume;
        }
        else
        {
            _settings = new GameSettings();
        }
    }

    private void OnEnable()
    {
        CharacterManager.OnPlayersCreated += SetPlayers;
        BattleManager.OnBattleEnded += EndBattle;
    }

    private void OnDisable()
    {
        CharacterManager.OnPlayersCreated -= SetPlayers;
        BattleManager.OnBattleEnded -= EndBattle;
    }

    #region SETTINGS
    public void SetGameConfig(int resX, int resY, bool fs, float vol)
    {
        Screen.SetResolution(resX, resY, fs);
        AudioListener.volume = vol;

        _settings.res_x = resX;
        _settings.res_y = resY;
        _settings.fullScreen = fs;
        _settings.volume = vol;

        string json = JsonUtility.ToJson(_settings);

        File.WriteAllText(_settingsPath, json);
    }
    #endregion

    #region SCENES
    public void GoToScene(string sceneName)
    {
        _sceneToLoad = sceneName;
        StartCoroutine(LoadScene());
    }

    public void GoToScene(string sceneName, int dimension_x, int dimension_z)
    {
        _sceneToLoad = sceneName;
        boardDimension_X = dimension_x;
        boardDimension_Z = dimension_z;

        StartCoroutine(LoadScene());
    }

    private IEnumerator LoadScene()
    {
        AsyncOperation sceneLoad = SceneManager.LoadSceneAsync("Loading");

        sceneLoad.allowSceneActivation = false;

        while (!sceneLoad.isDone)
        {
            if (sceneLoad.progress >= 0.9f)
            {
                sceneLoad.allowSceneActivation = true;
            }

            yield return null;
        }
    }
    #endregion

    #region MATCH
    public void SelectPlayer1Character(int index)
    {
        player1CharIndex = index;
    }

    public void SelectPlayer2Character(int index)
    {
        player2CharIndex = index;
    }

    public void SetPlayers(Player player1, Player player2)
    {
        players[0] = player1;
        players[1] = player2;

        currentPlayer = previousPlayer = players[0];
        currentPlayerIndex = 0;

        camera = Camera.main;

        camera.transform.position = currentPlayer.charInstance.GetComponent<CameraPointStorage>().cameraPoint.position;

        cameraGuide.transform.position = camera.transform.position;
        cameraGuide.transform.rotation = camera.transform.rotation;

        OnHealthChanged(player1, player2);

        StartCoroutine(CameraRoutine());
        StartCoroutine(MoveRoutine());
    }

    IEnumerator CameraRoutine()
    {
        while (true)
        {
            

            if (currentPlayer != previousPlayer)
            {
                Vector3 cameraPoint = currentPlayer.charInstance.GetComponent<CameraPointStorage>().cameraPoint.position;

                cameraGuide.transform.position = cameraPoint;

                while (camera.transform.position!= cameraPoint)
                {
                    camera.transform.position = Vector3.MoveTowards(camera.transform.position, cameraPoint, 5f * Time.deltaTime);
                    yield return null;
                }

                cameraGuide.LookAt(currentPlayer.charInstance.transform);
                float time = 0;
                while (camera.transform.rotation != cameraGuide.transform.rotation)
                {
                    if (time <= cameraLerpTime)
                    {
                        time += Time.deltaTime;
                        camera.transform.rotation = Quaternion.Lerp(camera.transform.rotation, cameraGuide.transform.rotation, time / cameraLerpTime);
                    }
                    else
                    {
                        camera.transform.rotation = cameraGuide.transform.rotation;
                        time = 0f;
                    }
                    yield return null;
                }
            }

            camera.transform.LookAt(currentPlayer.charInstance.transform);

            previousPlayer = currentPlayer;
            yield return null;
        }
    }

    private IEnumerator MoveRoutine()
    {
        while (true)
        {
            if (!battling && Input.GetMouseButtonDown(0))
            {
                BattleManager.instance.ShowDices(false);

                RaycastHit hit;
                Ray ray = camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << 8))
                {
                    if (hit.collider)
                    {
                        foreach (Tile t in currentPlayer.possibleTiles)
                        {
                            if (t.tileObj == hit.collider.gameObject)
                            {
                                if (currentPlayer.Move(t))
                                {

                                    while (currentPlayer.charInstance.transform.position.x != t.tileObj.transform.position.x ||
                                    currentPlayer.charInstance.transform.position.z != t.tileObj.transform.position.z)
                                    {
                                        currentPlayer.charInstance.transform.position = Vector3.MoveTowards(currentPlayer.charInstance.transform.position, t.tileObj.transform.position, charSpeed * Time.deltaTime);
                                        yield return null;
                                    }
                                    if (BoardManager.instance.IsEnemyNearby(currentPlayer))
                                    {
                                        StartBattle();
                                    }
                                    else if (currentPlayer.movesLeft == 0)
                                    {
                                        ChangePlayer();
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
            yield return null;
        }
    }

    private void StartBattle()
    {
        battling = true;
        BattleManager.instance.ShowDices(true, currentPlayerIndex);
    }

    private void EndBattle(List<int>[] result)
    {
        int p1Score = 0;
        int p2Score = 0;
        for (int x = 0; x < 3; x++)
        {
            if (result[0][x] > result[1][x])
            {
                p1Score++;
            }
            else if (result[0][x] < result[1][x])
            {
                p2Score++;
            }
            else
            {
                p1Score++;
                p2Score++;
            }
        }

        if (p1Score > p2Score)
        {
            players[0].Attack(players[1]);
        }
        else if (p2Score > p1Score)
        {
            players[1].Attack(players[0]);
        }
        else
        {
            if (currentPlayerIndex == 0)
                players[0].Attack(players[1]);
            else
                players[1].Attack(players[0]);
        }

        GetComponent<AudioSource>().Play();
        ChangePlayer();

        OnHealthChanged(players[0], players[1]);

        if (players[0].currentHp <= 0)
        {
            endGameWindow.SetActive(true);
            endGameMessage.text = "Player 2 wins!";
        }
        else if (players[1].currentHp <= 0)
        {
            endGameWindow.SetActive(true);
            endGameMessage.text = "Player 1 wins!";
        }
        battling = false;
    }

    public void PlayerRecovery()
    {
        OnHealthChanged(players[0],players[1]);
    }

    private void ChangePlayer()
    {
        currentPlayer.movesLeft = 3;

        currentPlayerIndex = (currentPlayerIndex + 1) % 2;
        currentPlayer = players[currentPlayerIndex];
    }

    public delegate void HealthEvents(Player p1, Player p2);
    public static event HealthEvents OnHealthChanged;
    #endregion


}


