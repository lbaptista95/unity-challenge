﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class BattleManager : MonoBehaviour
{
    public static BattleManager instance;

    [SerializeField] Sprite[] diceSprites;
    [SerializeField] private List<GameObject> dices;
    [SerializeField] private GameObject scorePanel;
    [SerializeField] private List<TMP_Text> diceScoreTexts;

    [SerializeField] private List<int>[] scores = new List<int>[2];
    [SerializeField] private int currentDiceIndex;

    [SerializeField] private Slider[] healthBars;
    [SerializeField] private TMP_Text[] playerTitles;
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }

        instance = this;
    }

    private void Start()
    {
        scorePanel.SetActive(false);
        foreach (GameObject dice in dices)
            dice.SetActive(false);

        scores[0] = new List<int>();
        scores[0] = new List<int>();
    }
    private void OnEnable()
    {
        CharacterManager.OnPlayersCreated += SetUI;
        GameManager.OnHealthChanged += UpdateHealthBars;
    }
    private void OnDisable()
    {
        CharacterManager.OnPlayersCreated -= SetUI;
        GameManager.OnHealthChanged += UpdateHealthBars;
    }

    private void SetUI(Player p1, Player p2)
    {
        dices[0].GetComponent<Image>().color = diceScoreTexts[0].color = playerTitles[0].color = healthBars[0].fillRect.GetComponent<Image>().color =
            p1.charInstance.GetComponentInChildren<MeshRenderer>().material.GetColor("_EmissionColor");

        dices[1].GetComponent<Image>().color = diceScoreTexts[1].color = playerTitles[1].color =  healthBars[1].fillRect.GetComponent<Image>().color =
            p2.charInstance.GetComponentInChildren<MeshRenderer>().material.GetColor("_EmissionColor");
    }

    private void UpdateHealthBars(Player player1, Player player2)
    {
        healthBars[0].maxValue = player1.maxHp;
        healthBars[0].value = player1.currentHp;

        healthBars[1].maxValue = player2.maxHp;
        healthBars[1].value = player2.currentHp;
    }

    public void ShowDices(bool state, int playerIndex = -1)
    {
        scorePanel.SetActive(state);

        if (playerIndex > -1)
        {
            if (state)
            {
                for (int x = 0; x < scores.Length; x++)
                    scores[x] = new List<int>();

                foreach (TMP_Text txt in diceScoreTexts)
                    txt.text = string.Empty;
            }

            currentDiceIndex = playerIndex;

            SelectDice(playerIndex);
        }
        else
        {
            foreach (GameObject dice in dices)
            {
                dice.SetActive(state);
            }
        }
    }


    public void SelectDice(int index)
    {
        foreach (GameObject dice in dices)
        {
            dice.SetActive(true);
            if (index > -1)
                dice.GetComponent<Button>().interactable = index == dices.IndexOf(dice);
        }
    }

    public void RollDice(GameObject dice)
    {
        dice.GetComponent<Animator>().enabled = true;

        dice.GetComponent<Animator>().SetBool("roll", true);

        dice.GetComponent<Button>().enabled = false;

        Image img = dice.GetComponent<Image>();

        StartCoroutine(DiceRolling(dice));
    }


    private IEnumerator DiceRolling(GameObject dice)
    {
        yield return new WaitForSeconds(2f);

        dice.GetComponent<Animator>().SetBool("roll", false);

        dice.GetComponent<Animator>().enabled = false;

        dice.GetComponent<Button>().enabled = true;

        Image img = dice.GetComponent<Image>();

        int value = Random.Range(1, 7);

        switch (value)
        {
            case 1:
                img.sprite = diceSprites[0];
                break;
            case 2:
                img.sprite = diceSprites[1];
                break;
            case 3:
                img.sprite = diceSprites[2];
                break;
            case 4:
                img.sprite = diceSprites[3];
                break;
            case 5:
                img.sprite = diceSprites[4];
                break;
            case 6:
                img.sprite = diceSprites[5];
                break;
        }

        scores[currentDiceIndex].Add(value);
        diceScoreTexts[currentDiceIndex].text += value + "\n\n";

        if (scores[currentDiceIndex].Count == 3)
        {
            currentDiceIndex = (currentDiceIndex + 1) % 2;
            if (scores[currentDiceIndex].Count < 3)
            {
                SelectDice(currentDiceIndex);
            }
            else
            {
                foreach (List<int> score in scores)
                {
                    for (int x = 0; x < score.Count; x++)
                    {
                        for (int y = score.Count - 1; y > x; y--)
                        {
                            if (score[x] < score[y])
                            {
                                int temp = score[x];
                                score[x] = score[y];
                                score[y] = temp;
                            }
                        }
                    }
                }

                OnBattleEnded.Invoke(scores);
            }
        }
    }

    public delegate void BattleEvents(List<int>[] result);
    public static event BattleEvents OnBattleEnded;
}
