﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GameSettings
{
    public int res_x;
    public int res_y;
    public bool fullScreen;

    public float volume;

    public GameSettings()
    {
        res_x = 1920;
        res_y = 1080;
        fullScreen = true;
        volume = 100;
    } 
}
