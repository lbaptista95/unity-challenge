﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Tile
{
    public bool hasPlayer;
    public GameObject tileObj;
    public Collectable collectable;
}

public class Board
{
    public Tile[,] tiles;

    public Board(int dimension_x, int dimension_z)
    {
        tiles = new Tile[dimension_x, dimension_z];

        for (int x = 0; x < dimension_x; x++)
        {
            for (int z = 0; z < dimension_z; z++)
            {
                tiles[x, z] = new Tile(); 
            }
        }
    }

    public void DistributeCollectables()
    {
        for (int x = 0; x < tiles.GetLength(0); x++)
        {
            for (int z = 0; z < tiles.GetLength(1); z++)
            {
                if (!tiles[x,z].hasPlayer)
                {
                    int type = Random.Range(0, 3);
                    tiles[x, z].collectable = Collectable.CreateInstance(type);
                }
            }
        }
    }
}


public abstract class Collectable
{
    public abstract void Increase(Player p);
   
    public GameObject instance;

    protected Collectable() { }

    public static Collectable CreateInstance(int type)
    {
        switch(type)
        {
            case 0: return new HpCollectable();
            case 1: return new AtkCollectable();
            case 2: return new MoveCollectable();

            default: return new HpCollectable();
        }
    }
}

public class HpCollectable : Collectable
{
    public int extraHp;

    public HpCollectable()
    {
        extraHp = 10;
    }
    public override void Increase(Player p)
    {
        p.currentHp += extraHp;

        if (p.currentHp > p.maxHp)
            p.maxHp = p.currentHp;
        GameManager.instance.PlayerRecovery();
        BoardManager.instance.DestroyCollectable(this);
    }
}

public class AtkCollectable : Collectable
{
    public int extraAtk;

    public AtkCollectable()
    {
        extraAtk = 10;
    }
    public override void Increase(Player p)
    {
        p.currentAtk += extraAtk;
        BoardManager.instance.DestroyCollectable(this);
    }
}

public class MoveCollectable : Collectable
{
    public int extraMoves;

    public MoveCollectable()
    {
        extraMoves = 1;
    }

    public override void Increase(Player p)
    {
        p.movesLeft += extraMoves;
        BoardManager.instance.DestroyCollectable(this);
    }
}