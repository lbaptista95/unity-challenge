﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character
{
    public int hp;
    public int atk;
    public GameObject prefab;
}

public class Player
{
    public int movesLeft;
    public int maxHp;
    public int currentHp;
    public int currentAtk;
    public Tile currentTile;
    public List<Tile> possibleTiles;
    public Character character;
    public GameObject charInstance;

    public Player(Character c)
    {
        character = c;
        currentHp = maxHp = c.hp;
        currentAtk = c.atk;
        movesLeft = 3;
    }

    public void Attack(Player enemy)
    {
        enemy.GetHit(currentAtk);
    }

    public void GetHit(int enemy_atk)
    {
        currentHp -= enemy_atk;
    }

    public void Initialize(Tile initialTile)
    {
        currentTile = initialTile;
        currentTile.hasPlayer = true;
        possibleTiles = BoardManager.instance.CheckPossibleTiles(this);
    }

    public bool Move(Tile destination)
    {
        bool canMove = false;
        if (movesLeft > 0)
        {
            possibleTiles = BoardManager.instance.CheckPossibleTiles(this);
            if (possibleTiles.Contains(destination))
            {
                currentTile.hasPlayer = false;
                currentTile = destination;
                currentTile.hasPlayer = true;

                if (currentTile.collectable != null)
                {
                    currentTile.collectable.Increase(this);
                    currentTile.collectable = null;
                }

                possibleTiles = BoardManager.instance.CheckPossibleTiles(this);
                movesLeft--;
                canMove = true;

            }
        }
        return canMove;
    }
}
